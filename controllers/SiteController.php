<?php

namespace app\controllers;

use app\models\Foo;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
  /**
   * Displays homepage.
   *
   * @return string
   */
  public function actionTest()
  {
    $model = Foo::findOne(1);
    if(!$model)return '';
    $model->delete();
    return $this->render('index');
  }

}
